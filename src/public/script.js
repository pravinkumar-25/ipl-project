function wonTossAndMatch(){

    let wonTeams = [];
    let matches = [];
    fetch("./output/teamsWonTossAndMatch.json")
        .then(response => response.json())
        .then(data =>{
            

            for(let teams in data){
                wonTeams.push(teams);
                matches.push(data[teams]);
            }
            
            
            Highcharts.chart('wonTossAndMatch', {
                chart: {
                    type: 'column',
                    options3d: {
                        enabled: true,
                        alpha: 10,
                        beta: 25,
                        depth: 100
                    }
                },
                title: {
                    text: '<h1>Teams that won Toss and Match</h1>'
                },
                subtitle: {
                    text: 'source : <a href="https://www.kaggle.com/manasgarg/ipl">Kaggle</a>'
                },
                plotOptions: {
                    column: {
                        depth: 25
                    }
                },
                xAxis: {
                    categories: wonTeams,
                    labels: {
                        skew3d: true,
                        style: {
                            fontSize: '16px'
                        }
                    }
                },
                yAxis: {
                    title: {
                        text: "No of matches"
                    }
                },
                series: [{
                    name: 'Won Toss and Match',
                    data: matches
                }]
            });

        });
}

wonTossAndMatch();


function matchesPerYear(){
    const matches = [];
    fetch("./output/matchesPerYear.json")
        .then(response => response.json())
        .then(data =>{
      
      for(let key in data){
          matches.push([key,data[key]]);
      }
      Highcharts.chart('matchesPerYear', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'IPL Matches Per Season'
        },
        subtitle: {
            text: 'Source: <a href="https://www.kaggle.com/manasgarg/ipl">Kaggle</a>'
        },
        xAxis: {
            type: 'category',
            labels: {
                rotation: -45,
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'No of IPL matches'
            }
        },
        legend: {
            enabled: false
        },
        tooltip: {
            pointFormat: 'No of IPL matches: <b>{point.y:.1f}</b>'
        },
        series: [{
            name: 'No od matches',
            data: matches,
            dataLabels: {
                enabled: true,
                rotation: -90,
                color: '#FFFFFF',
                align: 'right',
                format: '{point.y:.1f}', // one decimal
                y: 10, // 10 pixels down from the top
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        }]
    });
  });
}

matchesPerYear();

function matchesWonPerTeam(){
    let matches = [];
    let years =[];
    
    fetch("./output/matchesWonPerTeam.json")
        .then(response => response.json())
        .then(data =>{


            for(let teams in data){
                for(let year in data[teams]){
                    if(!years.includes(year)){
                        years.push(year);
                    }
                }
            }
            years.sort((firstValue,secondValue)=>{
                return firstValue-secondValue;
            });


            for(let teams in data){
                let wonArray = [];
                for(let index in years){
                    if(!data[teams][years[index]]){
                        wonArray.push(0);
                    }else{
                        wonArray.push(data[teams][years[index]]);
                    }
                }
                matches.push({
                    name: teams,
                    data: wonArray
                });
                
            }
            

            Highcharts.chart('matchesWonPerTeam', {
                chart: {
                    type: 'bar'
                },
                title: {
                    text: 'Matches won per team for each Season (2008 to 2017)'
                },
                subtitle: {
                    text: 'Source: <a href="https://www.kaggle.com/manasgarg/ipl">Kaggle</a>'
                },
                xAxis: {
                    categories: years,
                    title: {
                        text: "Years"
                    }
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'No of matches won',
                    },
                    labels: {
                        overflow: 'justify'
                    }
                },
                tooltip: {
                    valueSuffix: null
                },
                plotOptions: {
                    bar: {
                        dataLabels: {
                            enabled: true
                        }
                    }
                },
                legend: {
                    layout: 'vertical',
                    align: 'right',
                    verticalAlign: 'top',
                    x: -40,
                    y: 80,
                    floating: true,
                    borderWidth: 1,
                    backgroundColor:
                        Highcharts.defaultOptions.legend.backgroundColor || '#FFFFFF',
                    shadow: true
                },
                credits: {
                    enabled: false
                },
                series: matches
            });
        })
}

matchesWonPerTeam();

function extraRunsConceded(){
    let teams = [];
    fetch("./output/extraRunsConceded.json")
        .then(response => response.json())
        .then(data =>{
            
            for(let team in data){
                let object = {
                    name : team,
                    y : data[team]
                }
                teams.push(object);
            }

            Highcharts.chart('extraRunsConceded', {
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                },
                title: {
                    text: 'Extra runs conceded by a team in 2015'
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                },
                accessibility: {
                    point: {
                        valueSuffix: '%'
                    }
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: true,
                            format: '<b>{point.name}</b>: {point.percentage:.1f} %'
                        }
                    }
                },
                series: [{
                    name: 'Extra runs in 2015',
                    colorByPoint: true,
                    data: teams
                }]
            });
        });
}

extraRunsConceded();

function economicBowlers(){
    let players = [];
    let economy = [];
    fetch("./output/economicBowlers.json")
        .then(response => response.json())
        .then(data =>{
        
            for(let player in data){
                players.push(player);
                economy.push(data[player]);
            }

            Highcharts.chart('economicBowlers', {
                chart: {
                    type: 'bar'
                },
                title: {
                    text: 'Economic Players'
                },
                subtitle: {
                    text: 'Source:  <a href="https://www.kaggle.com/manasgarg/ipl">Kaggle</a>'
                },
                xAxis: {
                    categories: players,
                    title: {
                        text: 'Bowlers'
                    }
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'economy',
                    },
                    labels: {
                        overflow: 'justify'
                    }
                },
                tooltip: {
                    valueSuffix: ' millions'
                },
                plotOptions: {
                    bar: {
                        dataLabels: {
                            enabled: true
                        }
                    }
                },
                legend: {
                    layout: 'vertical',
                    align: 'right',
                    verticalAlign: 'top',
                    x: -40,
                    y: 80,
                    floating: true,
                    borderWidth: 1,
                    backgroundColor:
                        Highcharts.defaultOptions.legend.backgroundColor || '#FFFFFF',
                    shadow: true
                },
                credits: {
                    enabled: false
                },
                series: [{
                    name: 'economy',
                    data: economy
                }]
            });
        })
}

economicBowlers();

function playerOfTheMatch(){
    let playersData = [];
    fetch("./output/playerOfMatchPerSeason.json")
        .then(response => response.json())
        .then(data =>{

            for(let year in data){
                playersData.push([year+"-"+data[year].name,data[year].matches]);
            }
            Highcharts.chart('playerOfTheMatch', {
                chart: {
                    type: 'pie',
                    options3d: {
                        enabled: true,
                        alpha: 45
                    }
                },
                title: {
                    text: 'Highest number of player of the match per season'
                },
                subtitle: {
                    text: 'source : <a href="https://www.kaggle.com/manasgarg/ipl">Kaggle</a> '
                },
                plotOptions: {
                    pie: {
                        innerSize: 100,
                        depth: 45
                    }
                },
                series: [{
                    name: 'Number of Player of the match',
                    data: playersData
                }]
            });
        })

}

playerOfTheMatch();

function bestEconomy(){
    let runDetail = [];
    let players = [];
    let economy = [];
    fetch("./output/bestEconomy.json")
        .then(response => response.json())
        .then(data =>{
            runDetail = (Object.keys(data[Object.keys(data)[0]]));
            

            for(let player in data){
                players.push(player);
            }
            for(let detail in runDetail){
                let array = [];
                for(let player in data){
                    array.push(data[player][runDetail[detail]]);
                }
                economy.push({
                    name : runDetail[detail],
                    data : array
                })
            }
            

            Highcharts.chart('bestEconomy', {
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Best Economy Bowler in IPL'
                },
                xAxis: {
                    categories: players
                },
                credits: {
                    enabled: false
                },
                series: economy
            });
        })
}

bestEconomy();

function strikeRate(){
    let strikeRateOfBatsman = [];
    fetch("./output/strikeRateOfBatsman.json")
        .then(response => response.json())
        .then(data =>{
            
            for(let player in data){

                let strikeRates = [];
                for(let year in data[player]){
                    strikeRates.push(data[player][year]);
                }
                strikeRateOfBatsman.push({
                    name : player,
                    data : strikeRates
                })
            }
            Highcharts.chart('strikeRate', {

                title: {
                    text: 'Strike rate of MS Dhoni from 2008 to 2017'
                },
            
                subtitle: {
                    text: 'Source: <a href="https://www.kaggle.com/manasgarg/ipl">Kaggle</a>'
                },
            
                yAxis: {
                    title: {
                        text: 'Strike Rate'
                    }
                },
            
                xAxis: {
                    accessibility: {
                        rangeDescription: 'Range: 2008 to 2017'
                    },
                    title:{
                        text: 'Years'
                    }                    
                },
            
                legend: {
                    layout: 'vertical',
                    align: 'right',
                    verticalAlign: 'middle'
                },
            
                plotOptions: {
                    series: {
                        label: {
                            connectorAllowed: false
                        },
                        pointStart: 2008
                    }
                },
            
                series: strikeRateOfBatsman,
            
                responsive: {
                    rules: [{
                        condition: {
                            maxWidth: 500
                        },
                        chartOptions: {
                            legend: {
                                layout: 'horizontal',
                                align: 'center',
                                verticalAlign: 'bottom'
                            }
                        }
                    }]
                }
            
            });
        });
}

strikeRate();

function playerDismissed(){
    let player = [];
    fetch("./output/playerDismissed.json")
        .then(response => response.json())
        .then(data =>{

            player.push([data.name,data.dismissedTimes])
            console.log(player)
            Highcharts.chart('playerDismissed', {
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Highest number of times a player is dismissed by another player'
                },
                subtitle: {
                    text: 'Source: <a href="https://www.kaggle.com/manasgarg/ipl">Kaggle</a>'
                },
                xAxis: {
                    type: 'category',
                    labels: {
                        rotation: -45,
                        style: {
                            fontSize: '13px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    }
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: ''
                    }
                },
                legend: {
                    enabled: false
                },
                tooltip: {
                    pointFormat: 'Dismissals: <b>{point.y:.1f} dismissals</b>'
                },
                series: [{
                    name: 'Player dismissed',
                    data:player,
                    dataLabels: {
                        enabled: true,
                        rotation: -90,
                        color: '#FFFFFF',
                        align: 'right',
                        format: '{point.y:.1f}', // one decimal
                        y: 10, // 10 pixels down from the top
                        style: {
                            fontSize: '13px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    }
                }]
            });
        });
}

playerDismissed();