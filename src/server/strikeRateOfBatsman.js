function strikeRate(matches,deliveries,chosenPlayer){
    let strikeRateOfBatsman = {};
    let calculateStrikeRate = {};
    for(let index = 0 ; index < matches.length ; index++){
        if(!calculateStrikeRate[matches[index].season]){
            calculateStrikeRate[matches[index].season] = {};
        }
        for(let j = 0 ; j < deliveries.length ; j++){
            if(deliveries[j].match_id === matches[index].id){
                if(calculateStrikeRate[matches[index].season][deliveries[j].batsman]){
                    calculateStrikeRate[matches[index].season][deliveries[j].batsman].runs += parseInt(deliveries[j]['total_runs'])
                    calculateStrikeRate[matches[index].season][deliveries[j].batsman].balls += 1;
                }else{
                    calculateStrikeRate[matches[index].season][deliveries[j].batsman] = {
                        runs : parseInt(deliveries[j]['total_runs']),
                        balls : 1
                    }
                }
            }
        }
    }
    for(let season in calculateStrikeRate){
        strikeRateOfBatsman[season] = {};
        for(let batsman in calculateStrikeRate[season]){
            strikeRateOfBatsman[season][batsman] = (calculateStrikeRate[season][batsman].runs/calculateStrikeRate[season][batsman].balls)*100;
        }
    }
    let strikeRateOfChosenBatsman = {};
    strikeRateOfChosenBatsman[chosenPlayer] = {};
    for(let season in strikeRateOfBatsman){
        if(strikeRateOfBatsman[season][chosenPlayer]){
            strikeRateOfChosenBatsman[chosenPlayer][season] = strikeRateOfBatsman[season][chosenPlayer];
        }
        else{
            strikeRateOfChosenBatsman[chosenPlayer][season] = 0;
        }
    }

    return strikeRateOfChosenBatsman;
}
module.exports = strikeRate;