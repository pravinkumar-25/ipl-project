const csv = require('csvtojson');
const fs = require('fs');

csv().fromFile("../data/matches.csv")
    .then((data)=>{
        console.log(data);

        fs.writeFileSync("matches.json",JSON.stringify(data),"utf-8");
    });

csv().fromFile("../data/deliveries.csv")
    .then((data)=>{
        console.log(data);

        fs.writeFileSync("deliveries.json",JSON.stringify(data),"utf-8");
    });