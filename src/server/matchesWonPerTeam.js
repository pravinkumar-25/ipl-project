function matchesWonPerTeam(object){
    let wonPerTeam={};
    for(let i=0;i< object.length;i++){
        if(wonPerTeam[object[i].winner]){
            if(wonPerTeam[object[i].winner][object[i].season]){
                wonPerTeam[object[i].winner][object[i].season]+=1;
            }else{
                wonPerTeam[object[i].winner][object[i].season]=1;
            }
        }else{
            wonPerTeam[object[i].winner]={};
            wonPerTeam[object[i].winner][object[i].season]=1
        }
    }
    return wonPerTeam;
}
module.exports = matchesWonPerTeam;