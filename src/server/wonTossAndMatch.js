function wonTossAndMatch(object){
    let wonTimes = {};
    for(let index =0;index< object.length; index++){
        if(object[index].winner === object[index]['toss_winner']){
            if(wonTimes[object[index].winner]){
                wonTimes[object[index].winner] += 1;
            }else{
                wonTimes[object[index].winner] = 1;
            }
        }
    }
    return wonTimes;
}
module.exports = wonTossAndMatch;