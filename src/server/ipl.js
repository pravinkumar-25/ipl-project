const matches = require("./matches.json");
const deliveries = require("./deliveries.json");

const matchesPerYear = require("./matchesPerYear");
let match = matchesPerYear(matches);

const matchesWonPerTeam = require("./matchesWonPerTeam");
const wonPerTeam = matchesWonPerTeam(matches);

const extraRunsConceded = require("./extraRunsConceded");
const extraRuns = extraRunsConceded(matches,deliveries);

const economicBowlers = require("./economicBowlers");
const bowlers = economicBowlers(matches,deliveries);

const wonTossAndMatch = require("./wonTossAndMatch");
const wonTimes = wonTossAndMatch(matches);

const playerOfMatch = require("./playerOfTheMatchPerSeason");
const playersPerSeason = playerOfMatch(matches);

const strikeRateOfBatsman = require("./strikeRateOfBatsman");
const strikeRate = strikeRateOfBatsman(matches,deliveries,"MS Dhoni");

const bestEconomy = require("./bestEconomyBowler");
const bestEconomyBowler = bestEconomy(deliveries);

const playerDismissed = require("./playerDismissed");
const numberOfTimesDismissed = playerDismissed(deliveries);

const fs = require('fs');
fs.writeFileSync("../public/output/matchesPerYear.json",JSON.stringify(match),null,'\n');
fs.writeFileSync("../public/output/matchesWonPerTeam.json",JSON.stringify(wonPerTeam),null,'\n');
fs.writeFileSync("../public/output/extraRunsConceded.json",JSON.stringify(extraRuns),null,'\n');
fs.writeFileSync("../public/output/economicBowlers.json",JSON.stringify(bowlers),null,'\n');
fs.writeFileSync("../public/output/teamsWonTossAndMatch.json",JSON.stringify(wonTimes),null,'\n');
fs.writeFileSync("../public/output/playerOfMatchPerSeason.json",JSON.stringify(playersPerSeason),null,'\n');
fs.writeFileSync("../public/output/strikeRateOfBatsman.json",JSON.stringify(strikeRate),null,'\n');
fs.writeFileSync("../public/output/bestEconomy.json",JSON.stringify(bestEconomyBowler),null,'\n');
fs.writeFileSync("../public/output/playerDismissed.json",JSON.stringify(numberOfTimesDismissed),null,'\n');