function extraRunsConceded(matches,deliveries){
    let extraRuns = {};
    for(let i=0;i< matches.length;i++){
        if(matches[i].season === '2016'){
            
            for(let j=0;j<deliveries.length;j++){
                if(deliveries[j].match_id === matches[i].id){
                    if(extraRuns[deliveries[j].bowling_team]){
                        extraRuns[deliveries[j].bowling_team] += parseInt(deliveries[j].extra_runs);
                    }else{
                        extraRuns[deliveries[j].bowling_team] = parseInt(deliveries[j].extra_runs);
                    }
                }
            }
        }
    }
    return extraRuns;
}
module.exports = extraRunsConceded;