function matchesPerYear(object){
    let noOfMatches = {};
    for(let i=0;i< object.length;i++){
        if(noOfMatches[object[i].season]){
            noOfMatches[object[i].season]+=1;
        }else{
            noOfMatches[object[i].season]=1;
        }
    }
    return noOfMatches;
}
module.exports = matchesPerYear;