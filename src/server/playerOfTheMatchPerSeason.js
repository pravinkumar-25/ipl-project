function playerOfTheMatch(object){
    let players = {};
    let playersPerSeason = {};
    for(let index = 0 ; index < object.length ; index++){
        if(playersPerSeason[object[index].season]){
            if(playersPerSeason[object[index].season][object[index]['player_of_match']]){
                playersPerSeason[object[index].season][object[index]['player_of_match']] += 1;
            }else{
                playersPerSeason[object[index].season][object[index]['player_of_match']] = 1;
            }
        }else{
            playersPerSeason[object[index].season] = {};
            playersPerSeason[object[index].season][object[index]['player_of_match']] = 1;
        }
    }
    for(let season in playersPerSeason){
        players[season] = [];
        for(let player in playersPerSeason[season]){
            players[season].push([player,playersPerSeason[season][player]]);
        }
        players[season].sort((firstValue, secondValue) =>{
            return secondValue[1]-firstValue[1];
        })
        playersPerSeason[season] = {
            name : players[season][0][0],
            matches : players[season][0][1]
        }
    }
    return playersPerSeason;
}
module.exports = playerOfTheMatch;