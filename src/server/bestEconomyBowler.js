function bestEconomy(deliveries){
    let bestEconomyBowler = {};
    let bowlers = {};
    for(let index = 0; index < deliveries.length ; index++){
        if(deliveries[index]['is_super_over'] === '1'){
            if(bowlers[deliveries[index].bowler]){
                if(bowlers[deliveries[index].bowler].matchId !== deliveries[index]['match_id']){
                    bowlers[deliveries[index].bowler].matchId = deliveries[index]['match_id'];
                    bowlers[deliveries[index].bowler].overs += 1;
                }
                bowlers[deliveries[index].bowler].runs += parseInt(deliveries[index]['total_runs']);
            }else{
                bowlers[deliveries[index].bowler] = {
                    runs : parseInt(deliveries[index]['total_runs']),
                    overs : 1,
                    matchId : deliveries[index]['match_id']
                }
            }
        }
    }
    let maxNumber = Number.MAX_VALUE;
    for(const player in bowlers){
        bowlers[player]['economy'] = bowlers[player].runs / bowlers[player].overs;
        delete bowlers[player].matchId;
        if(bowlers[player]['economy'] < maxNumber){
            maxNumber = bowlers[player]['economy'];
        }
    }
    for(const player in bowlers){
        if(bowlers[player].economy === maxNumber){
            bestEconomyBowler[player] = bowlers[player];
        }
    }
    return bestEconomyBowler;
}
module.exports = bestEconomy;