function playerDismissed(deliveries){
    let numberOfTimes = {};

    for (let index = 0; index < deliveries.length; index++) {
        if (deliveries[index]['player_dismissed'] && deliveries[index]["dismissal_kind"] != "retired hurt" && deliveries[index]["dismissal_kind"] != "obstructing the field") {
            if (numberOfTimes[deliveries[index]['player_dismissed']]) {
                if (deliveries[index]['feilder']) {
                    if (numberOfTimes[deliveries[index]['player_dismissed']][deliveries[index]['fielder']]) {
                        numberOfTimes[deliveries[index]['player_dismissed']][deliveries[index]['fielder']] += 1;
                    } else {
                        numberOfTimes[deliveries[index]['player_dismissed']][deliveries[index]['fielder']] = 1;
                    }

                } else {

                    if (numberOfTimes[deliveries[index]['player_dismissed']][deliveries[index]['bowler']]) {
                        numberOfTimes[deliveries[index]['player_dismissed']][deliveries[index]['bowler']] += 1;
                    } else {
                        numberOfTimes[deliveries[index]['player_dismissed']][deliveries[index]['bowler']] = 1;
                    }

                }
            } else {
                numberOfTimes[deliveries[index]['player_dismissed']] = {};
                if (deliveries[index]['fielder']) {
                    numberOfTimes[deliveries[index]['player_dismissed']][deliveries[index]['fielder']] = 1;
                } else {
                    numberOfTimes[deliveries[index]['player_dismissed']][deliveries[index]['bowler']] = 1;
                }
            }
        }
    }

    let max = 0;
    let batsman ;
    for (const player in numberOfTimes) {
        let array = Object.entries(numberOfTimes[player]);
        for (let index = 0; index < array.length; index++) {
            if (max < array[index][1]) {
                max = array[index][1];
                batsman = player;
            }
        }
    }

    return {
        name : batsman,
        dismissedTimes : max
    };
}

module.exports = playerDismissed;