function economicBowlers(matches,deliveries){
    let economicBowlers ={};
    let bowlers ={};
    for(let i=0;i< matches.length;i++){
        if(matches[i].season === '2015'){
            for(let j=0;j<deliveries.length;j++){
                if(deliveries[j].match_id === matches[i].id){
                    if(bowlers[deliveries[j].bowler]){
                        if(deliveries[j].ball === '1'){
                            bowlers[deliveries[j].bowler]['overs']+=1;
                        }
                        bowlers[deliveries[j].bowler].runs += parseInt(deliveries[j].total_runs);
                        if(bowlers[deliveries[j].bowler].runs > 0){
                        bowlers[deliveries[j].bowler].economy = bowlers[deliveries[j].bowler].runs/bowlers[deliveries[j].bowler].overs;
                        }
                    }else{
                        bowlers[deliveries[j].bowler] = {
                            'runs' : parseInt(deliveries[j].total_runs),
                            'overs': 1,
                            'economy':parseInt(deliveries[j].total_runs)
                        }
                    }
                }
            }
        }
    }
    let sortedBowlers = [];
    for(let key in bowlers){
        sortedBowlers.push([key,bowlers[key]['economy']]);
    }
    
    sortedBowlers.sort(function(a, b){
        return (a[1]-b[1]);
    });
    for(let i=0;i<10;i++){
        economicBowlers[sortedBowlers[i][0]]=sortedBowlers[i][1];
    }
    return economicBowlers;
}
module.exports = economicBowlers;